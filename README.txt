Audero Smoke Effect

---------------------------
GENERAL INFO
---------------------------

Author:     Aurelio De Rosa <aurelioderosa@gmail.com>
Version:    1.0
License:    http://creativecommons.org/licenses/by/3.0/ Creative Commons BY 3.0
Link:       https://bitbucket.org/AurelioDeRosa/auderosmokeeffect

---------------------------
DESCRIPTION
---------------------------

"Audero Smoke Effect" is a JavaScript library that let you create a smoke effect for one
or more elements on your web page. Several little clouds will be created starting from a
choosen element(s). This script requires jQuery in order to work and so jQuery must be
included before it. Moreover, the code is correlated with a CSS file that must be included
in the page in which you want to use the effect. The library is based on the idea shown by Gaya
(http://www.gayadesign.com/diy/puffing-smoke-effect-in-jquery/) but the code has been totally
written from scratch and has new features.

---------------------------
WARRANTY
---------------------------
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.