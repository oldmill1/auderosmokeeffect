/**
 * Description of AuderoSmokeEffect
 *
 * "Audero Smoke Effect" is a JavaScript library that let you create a smoke effect
 * for one or more elements on your web page.
 * Several little clouds will be created starting from a choosen element(s).
 * This script <b>requires jQuery</b> in order to work and so jQuery must be included
 * <b>before</b> it. Moreover, the code is correlated with a CSS file that
 * must be included in the page in which you want to use the effect.
 * The library is based on the idea shown by Gaya 
 * <a target="_blank" href="http://www.gayadesign.com/diy/puffing-smoke-effect-in-jquery/">here</a>
 * but <u>the code has been totally written from scratch and has new features.</u>
 * For more details on this library and how to use it, see the official documentation.
 * 
 * LICENSE: Permission is granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * @author     Aurelio De Rosa <aureliodersa@gmail.com>
 * @version    1.0
 * @license    http://creativecommons.org/licenses/by/3.0/ CC BY 3.0
 * @link       https://bitbucket.org/AurelioDeRosa/auderosmokeeffect
 */

var SmokeEffect = {

	Options: {			
		ImagePath: "", // The path to locate the smoke image
		Width: 70, // The width of the image shown to simulate the smoke
		Height: 50, // The height of the image shown to simulate the smoke
		Repeat: -1, // The number of times to repeat the animation. -1 means unlimited
		Pause: 2000, // The time (milliseconds) between animations. Set to "random" to have random time
		Speed: 4000 // The time taken (milliseconds) by the animation
	},

	Counter: new Object(),	
	SmokePos: new Object(),
	SmokeOptions: new Object(),
	Timers: new Object(),

	/**
    *	Override the default settings
	 * @param  ParentId  (string) The id of the parent used to attach the animation
	 * @param  Options  (object) The new settings to use as defaults (if some options are not
	 * specified for that options the default value will be used)
	 */
	smokeEffect: function(ParentId, Options)
	{
		if (this.SmokeOptions[ParentId] != undefined && this.SmokeOptions[ParentId] != null)
			this.SmokeOptions[ParentId] = null;

		this.SmokeOptions[ParentId] = $.extend({}, this.Options);
		if (Options != null)
			$.extend(this.SmokeOptions[ParentId], Options);
	},

	/**
    *	The method used to run the animation
	 * @param  ParentId  (string) The id of the parent used to attach the animation
	 * @param  PosX  (int) The X coordinate used as start point for the animation
	 * @param  PosY  (int) The Y coordinate used as start point for the animation
	 */
	run: function(ParentId, PosX, PosY)
	{
		// Save the position from which starts the puff
		// based on the parent element
		if (this.SmokePos[ParentId] == undefined)
			this.SmokePos[ParentId] = {"x": PosX, "y": PosY};

		if (this.Counter[ParentId] == undefined)
			this.Counter[ParentId] = 0;

		var SmokePuff = document.createElement("img");
		var PuffId = ParentId + "-smoke-puff-" + this.Counter[ParentId];

		// Create the smoke puff
		$(SmokePuff).addClass("smoke-puff")
		$(SmokePuff).attr(
			{
				"src": this.SmokeOptions[ParentId].ImagePath,
				"alt": "smoke puff",
				"id": PuffId
			}
		);

		var Position = $('#' + ParentId).offset();
		$(SmokePuff).css({
			top: (Position['top'] + this.SmokePos[ParentId]["y"]) + "px",
			left: (Position['left'] + this.SmokePos[ParentId]["x"]) + "px",
		});

		// Append the smoke puff to the body
		$("body").append($(SmokePuff));
		
		// Run smoke animation
		$(SmokePuff).animate(
			{
				width: this.SmokeOptions[ParentId].Width + "px",
				height: this.SmokeOptions[ParentId].Height + "px",
				marginLeft: "-" + (this.SmokeOptions[ParentId].Width / 2) + "px",
				marginTop: "-" + (this.SmokeOptions[ParentId].Height * 1.5) + "px",
				opacity: 0.9
			},
			this.SmokeOptions[ParentId].Speed * (1/3)
		);
		$(SmokePuff).animate(
			{
				marginTop: "-" + (this.SmokeOptions[ParentId].Height * 5) + "px",
				opacity: 0
			},
			this.SmokeOptions[ParentId].Speed * (2/3)
		);

		this.Counter[ParentId]++;
		// If the Pause options is "random", then a random time is generated
		var Time;
		if (this.SmokeOptions[ParentId].Pause == "random")
			Time = this.SmokeOptions[ParentId].Speed * (1/3) + Math.round(Math.random() * 2000);
		else
			Time = this.SmokeOptions[ParentId].Pause;

		// If the repetition limit is not reached, the animation method will run again
		if (this.SmokeOptions[ParentId].Repeat == -1 || this.SmokeOptions[ParentId].Repeat > this.Counter[ParentId])
		{
			if (this.Timers[ParentId] == undefined)
				this.Timers[ParentId] = new Array();
			this.Timers[ParentId].push( setTimeout("SmokeEffect.run('" + ParentId + "', " + PosX + ", " + PosY + ")", Time) );
		}
		
		// Set the time in the future where the puff will be deleted
		setTimeout("$('#" + PuffId + "').remove()", this.SmokeOptions[ParentId].Speed);
	},

	/**
    *	Stop one or more animations
	 * @param  [ParentId]  (array|string) This parameter is optional. If not specified, this method will
	 * stop all the animations. If specified it can be an array or a string. If an array is given, it has
	 * to contain the ids of the parent of the animation to stop. Otherwise, an id of a parent is required.
	 */
	stop: function(ParentId)
	{
		if (ParentId == undefined)
		{
			for(var Key in this.Timers)
				this.stop(Key);
		}
		else if ($.isArray(ParentId))
		{
			for(i = 0; i < this.ParentId.length; i++)
				this.stop(this.ParentId[i]);
		}
		else
		{
			if (this.Timers[ParentId] == undefined)
				return;

			for(i = 0; i < this.Timers[ParentId].length; i++)
				clearTimeout(this.Timers[ParentId][i]);
			this.Timers[ParentId].splice(0);
		}
	},

	/**
    *	Clear the resources associated to one or more animations. This method will also call <i>stop</i>
	 * before clear the resources.
	 * @param  [ParentId]  (array|string) This parameter is optional. If not specified, this method will
	 * clear all the animations resources. If specified it can be an array or a string. If an array is given, it has
	 * to contain the ids of the parent of the animation to clear. Otherwise, an id of a parent is required.
	 */
	clear: function(ParentId)
	{
		if (ParentId == undefined)
		{
			for(var Key in this.Timers)
				this.clear(Key);
		}
		else if ($.isArray(ParentId))
		{
			for(i = 0; i < this.ParentId.length; i++)
				this.clear(this.ParentId[i]);
		}
		else
		{
			if (this.Timers[ParentId] == undefined)
				return;
			this.stop(ParentId);

			delete this.SmokePos[ParentId];
			delete this.SmokeOptions[ParentId];
			delete this.Timers[ParentId];
		}
	}
}